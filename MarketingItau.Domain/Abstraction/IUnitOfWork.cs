﻿namespace MarketingItau.Domain.Abstraction
{
    public interface IUnitOfWork
    {
        ICustomersRepository CustomersRepository { get; }
        Task CommitAsync();
    }
}
