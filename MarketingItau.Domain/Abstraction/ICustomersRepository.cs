﻿using MarketingItau.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketingItau.Domain.Abstraction
{
    public interface ICustomersRepository
    {
        Task<IEnumerable<Customers>> GetCustomers();
        Task<Customers> GetCustomersById(int id);
        Task<Customers> AddCustomers(Customers customer);
        void UpdateCustomers(Customers customer);
        Task<Customers> DeleteCustomers(int id);
    }
}
