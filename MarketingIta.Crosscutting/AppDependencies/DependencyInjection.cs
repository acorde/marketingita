﻿using MarketingIta.Infrastructure.Context;
using MarketingIta.Infrastructure.Mapper;
using MarketingIta.Infrastructure.Repositories;
using MarketingItau.Domain.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MarketingIta.Crosscutting.AppDependencies
{

    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services,
                                                            IConfiguration configuration)
        {
            var myOracleConnection = configuration.GetConnectionString("DefaultConnection");
            //Registrar o serviço do contexto
            services.AddDbContext<AppDbContext>(options =>
                                                options.UseOracle(myOracleConnection).LogTo(Console.WriteLine, LogLevel.Information));
            services.AddScoped<ICustomersRepository, CustomersRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            var myhandlers = AppDomain.CurrentDomain.Load("MarketingIta.Application");
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(myhandlers));
            return services;
        }
    }

}
