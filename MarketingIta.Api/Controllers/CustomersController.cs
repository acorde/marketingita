﻿using AutoMapper;
using MarketingIta.Api.Dto;
using MarketingIta.Application.Customer.Command;
using MarketingItau.Domain.Model;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace MarketingIta.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ILogger<CustomersController> _logger;
        private readonly IMediator _mediator;

        public CustomersController(ILogger<CustomersController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        
        [HttpPost("Itau")]
        public async Task<IActionResult> AddCustomersItau(CreateCustomersCommand createCustomersCommand)
        {
            try
            {
                //CreateCustomersCommand createCustomersCommand = new CreateCustomersCommand();
                var createCustomer = await _mediator.Send(createCustomersCommand);
                if (createCustomer == null)
                {
                    return BadRequest();
                }
                else
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Api Marketing Itaú - Ocorreu uma exceção: " + ex.Message);
                return new StatusCodeResult(500);
            }

        }
    }
}
