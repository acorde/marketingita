﻿namespace MarketingIta.Api.Dto
{
    public class CustomersDto
    {
        public CustomersDto()
        {
            AddressDto = new AddressDto();
        }

        public string? CodigoHtml { get; set; }
        public string? CodigoInterno { get; set; }
        public string? CnpjParametro { get; set; }
        public string? CnpjConsultado { get; set; }
        public string? NumeroInscricao { get; set; }
        public string? NomeEmpresarial { get; set; }
        public string? InscricaoEstadual { get; set; }
        public string? ClientName { get; set; }
        public AddressDto AddressDto { get; set; }

        public CustomersDto(string? codigoHtml, string? codigoInterno, string? cnpjParametro, string? cnpjConsultado
            , string? numeroInscricao, string? nomeEmpresarial, string? inscricaoEstadual, string? clienteName, AddressDto addressDto)
        {
            CodigoHtml = codigoHtml;
            CodigoInterno = codigoInterno;
            CnpjParametro = cnpjParametro;
            CnpjConsultado = cnpjConsultado;
            NumeroInscricao = numeroInscricao;
            NomeEmpresarial = nomeEmpresarial;
            InscricaoEstadual = inscricaoEstadual;
            ClientName = clienteName;
            AddressDto = addressDto;
        }
    }
}
