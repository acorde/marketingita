﻿namespace MarketingIta.Api.Dto
{
    public class AddressDto
    {
        public string? UfParametro { get; set; }
        public string? Logradouro { get; set; }
        public string? Numero { get; set; }
        public string? Complemento { get; set; }
        public string? CEP { get; set; }
        public string? Bairro { get; set; }
        public string? Municipio { get; set; }
        public string? UF { get; set; }
        public string? CodigoIbge { get; set; }
        public int CustomersId { get; set; }
    }
}
