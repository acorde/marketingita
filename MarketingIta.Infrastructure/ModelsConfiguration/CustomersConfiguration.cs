﻿using MarketingItau.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MarketingIta.Infrastructure.ModelsConfiguration
{
    public class CustomersConfiguration : IEntityTypeConfiguration<Customers>
    {
        public void Configure(EntityTypeBuilder<Customers> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(m => m.CodigoHtml);
            builder.Property(m => m.CodigoInterno);
            builder.Property(m => m.CnpjParametro);
            builder.Property(m => m.CnpjConsultado);
            builder.Property(m => m.NumeroInscricao);
            builder.Property(m => m.NomeEmpresarial);
            builder.Property(m => m.InscricaoEstadual);            
        }
    }
}
