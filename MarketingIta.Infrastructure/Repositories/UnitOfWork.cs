﻿using MarketingIta.Infrastructure.Context;
using MarketingItau.Domain.Abstraction;

namespace MarketingIta.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private ICustomersRepository? _customerRepo;
        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        public ICustomersRepository CustomersRepository
        {
            get
            {
                return _customerRepo = _customerRepo ??
                    new CustomersRepository(_context);
            }
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
