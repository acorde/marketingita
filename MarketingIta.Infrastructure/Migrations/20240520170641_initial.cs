﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MarketingIta.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    CodigoHtml = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CodigoInterno = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CnpjParametro = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CnpjConsultado = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    NumeroInscricao = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    NomeEmpresarial = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    InscricaoEstadual = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    ClientName = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    Id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    UfParametro = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Logradouro = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Numero = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Complemento = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CEP = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Bairro = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Municipio = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    UF = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CodigoIbge = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CustomersId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Address_Customers_CustomersId",
                        column: x => x.CustomersId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Address_CustomersId",
                table: "Address",
                column: "CustomersId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}
