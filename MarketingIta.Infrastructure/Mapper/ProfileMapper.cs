﻿using AutoMapper;

namespace MarketingIta.Infrastructure.Mapper
{
    public class ProfileMapper<T, U> : Profile
    {
        public ProfileMapper() 
        {
            CreateMap<T, U>();
        }
    }
}
