﻿using MarketingItau.Domain.Model;
using MediatR;

namespace MarketingIta.Application.Customer.Command
{
    public class CreateCustomersCommand : IRequest<Customers>
    {
        public string? CodigoHtml { get; set; }
        public string? CodigoInterno { get; set; }
        public string? CnpjParametro { get; set; }
        public string? CnpjConsultado { get; set; }
        public string? NumeroInscricao { get; set; }
        public string? NomeEmpresarial { get; set; }
        public string? InscricaoEstadual { get; set; }
        public string? ClientName { get; set; }
        public Address Address { get; set; }
    }
}
