﻿using MarketingItau.Domain.Abstraction;
using MarketingItau.Domain.Model;
using MediatR;

namespace MarketingIta.Application.Customer.Command
{
    public class CreateCustomersCommandHandler : IRequestHandler<CreateCustomersCommand, Customers>
    {
        private readonly IUnitOfWork _unitOfWork;
        //
        public CreateCustomersCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Customers> Handle(CreateCustomersCommand request, CancellationToken cancellationToken)
        {
            var customers = new Customers(request.CodigoHtml, request.CodigoInterno, request.CnpjParametro, request.CnpjConsultado,
                                          request.NumeroInscricao, request.NomeEmpresarial, request.InscricaoEstadual, request.ClientName,
                                          request.Address);
            await _unitOfWork.CustomersRepository.AddCustomers(customers);
            await _unitOfWork.CommitAsync();

            return customers;
        }
    }
}
